# VUE 项目开发手册

## 目录

> 一、[相关技术与框架](#相关技术与框架)
>
> 二、[开发准备](#开发准备)
>
> > 1. [下载架构模板](#下载架构模板)
> >
> > 2. [安装依赖](#安装依赖)
> >
> > 3. [本地启动](#本地启动)
> >
> > 4. [打包编译](#打包编译)
>
> 三、[目录结构](#目录结构)
>
> 四、[代码编写规范](#代码编写规范)
>
> > 1. [基本规范](#1-基本规范)
> >
> > 2. [Vue 规范](#2-vue规范)
> >
> > 3. [Vuex 规范](#3-vuex规范)
>
> 五、[配置文件](#配置文件)
>
> 六、[分支管理](#分支管理)

## 相关技术与框架

| 名称       | 描述                            |
| ---------- | ------------------------------- |
| Vue        | 构建用户界面的渐进式框架        |
| Vuex       | 状态管理                        |
| Vue-Router | 路由管理                        |
| Vue-i18n   | 国际化实现                      |
| Element-UI | 基于 Vue 的样式组件库           |
| axios      | 基于 Promise 实现的 http 请求库 |

## 开发准备

###  安装依赖

```
yarn install
```

###  本地启动

```
yarn run serve
```

###  打包编译

```
yarn run build
```

## 目录结构

```
vue-template
├── dist // 编译输出目录
├── public // 放置任何需要绝对路径引入的静态资源
├── src
│   ├── api // 封装后的 API 接口
│   ├── assets // 直接引用的静态资源
│   ├── components // 通用组件
│   ├── pages  // 页面
│   ├── plugins // 通用插件
│   ├── router  // 路由管理
│   ├── store  // 状态管理
│   ├── utils  // 通用工具方法
│   ├── App.vue
│   ├── main.js // 项目入口文件
├── .browserslistrc // 浏览器兼容列表
├── .editorconfig // 编辑器配置
├── .env // 环境变量配置
├── .eslintrc // eslint配置
├── .gitignore // git上传忽略列表
├── babel.config.js // babel配置
├── package.json
├── postcss.config.js // postcss配置
├── vue.config.js // 项目vue配置
```

## 代码编写规范

### 1. 基本规范

HTML，CSS，JavaScript 等遵循部门前端规范

### 2. Vue 规范

#### 2.1 组件名为多个单词

**组件名应该始终是多个单词的，根组件 `App` 以及 `<transition>`、`<component>` 之类的 Vue 内置组件除外。**

这样做可以避免跟现有的以及未来的 HTML 元素相冲突，因为所有的 HTML 元素名称都是单个单词的。

```javascript
// bad
Vue.component('todo', {
  // ...
})

export default {
  name: 'Todo',
  // ...
}

// good
Vue.component('todo-item', {
  // ...
})

export default {
  name: 'TodoItem',
  // ...
}
```

#### 2.2 Prop 定义

**Prop 定义应该尽量详细，至少需要指定其类型。**

```javascript
// bad
props: ['status']

// good
props: {
  status: String
}

// best
props: {
  status: {
    type: String,
    required: true,
    validator: function (value) {
      return [
        'syncing',
        'synced',
        'version-conflict',
        'error'
      ].indexOf(value) !== -1
    }
  }
}
```

#### 2.3 为 v-for 设置键值

**总是用 key 配合 v-for。**

在组件上总是必须用 key 配合 v-for，以便维护内部组件及其子树的状态。甚至在元素上维护可预测的行为，比如动画中的对象固化 (object constancy)，也是一种好的做法。

```html
// bad
<ul>
  <li v-for="todo in todos">
    {{ todo.text }}
  </li>
</ul>

// good
<ul>
  <li v-for="todo in todos" :key="todo.id">
    {{ todo.text }}
  </li>
</ul>
```

#### 2.4 避免 v-if 和 v-for 用在一起

**永远不要把 v-if 和 v-for 同时用在同一个元素上。**

一般我们在两种常见的情况下会倾向于这样做：

1. 为了过滤一个列表中的项目 (比如 v-for="user in users" v-if="user.isActive")。在这种情形下，请将 users 替换为一个计算属性 (比如 activeUsers)，让其返回过滤后的列表。

2. 为了避免渲染本应该被隐藏的列表 (比如 v-for="user in users" v-if="shouldShowUsers")。这种情形下，请将 v-if 移动至容器元素上 (比如 ul, ol)。

```html
// bad
<ul>
  <li v-for="user in users" v-if="user.isActive" :key="user.id">
    {{ user.name }}
  </li>
</ul>
<ul>
  <li v-for="user in users" v-if="shouldShowUsers" :key="user.id">
    {{ user.name }}
  </li>
</ul>

// good
<ul>
  <li v-for="user in activeUsers" :key="user.id">
    {{ user.name }}
  </li>
</ul>
<ul v-if="shouldShowUsers">
  <li v-for="user in users" :key="user.id">
    {{ user.name }}
  </li>
</ul>
```

#### 2.5 私有属性名

**使用模块作用域保持不允许外部访问的函数的私有性。如果无法做到这一点，就始终为插件、混入等不考虑作为对外公共 API 的自定义私有属性使用 `$_` 前缀。并附带一个命名空间以回避和其它作者的冲突 (比如 `$_yourPluginName_`)。**

```javascript
// bad
var myGreatMixin = {
  // ...
  methods: {
    update: function () {
      // ...
    },
  },
}
var myGreatMixin = {
  // ...
  methods: {
    _update: function () {
      // ...
    },
  },
}
var myGreatMixin = {
  // ...
  methods: {
    $update: function () {
      // ...
    },
  },
}
var myGreatMixin = {
  // ...
  methods: {
    $_update: function () {
      // ...
    },
  },
}

// good
var myGreatMixin = {
  // ...
  methods: {
    $_myGreatMixin_update: function () {
      // ...
    },
  },
}

// best
var myGreatMixin = {
  // ...
  methods: {
    publicMethod() {
      // ...
      myPrivateFunction()
    },
  },
}

function myPrivateFunction() {
  // ...
}

export default myGreatMixin
```

#### 2.6 组件文件

**只要有能够拼接文件的构建系统，就把每个组件单独分成文件。**

当你需要编辑一个组件或查阅一个组件的用法时，可以更快速的找到它。

```javascript
// bad
Vue.component('TodoList', {
  // ...
})

Vue.component('TodoItem', {
  // ...
})

// good
components/
|- TodoList.js
|- TodoItem.js
components/
|- TodoList.vue
|- TodoItem.vue
```

#### 2.7 单文件组件文件的大小写

**单文件组件的文件名应该要么始终是单词大写开头 (PascalCase)，要么始终是横线连接 (kebab-case)。**

单词大写开头对于代码编辑器的自动补全最为友好，因为这使得我们在 JS(X) 和模板中引用组件的方式尽可能的一致。然而，混用文件命名方式有的时候会导致大小写不敏感的文件系统的问题，这也是横线连接命名同样完全可取的原因。

```
// bad
components/
|- mycomponent.vue
components/
|- myComponent.vue

// good
components/
|- MyComponent.vue
components/
|- my-component.vue
```

#### 2.8 基础组件名

**应用特定样式和约定的基础组件 (也就是展示类的、无逻辑的或无状态的组件) 应该全部以一个特定的前缀开头，比如 `Base`、`App` 或 `V`。**

```
// bad
components/
|- MyButton.vue
|- VueTable.vue
|- Icon.vue

// good
components/
|- BaseButton.vue
|- BaseTable.vue
|- BaseIcon.vue
components/
|- AppButton.vue
|- AppTable.vue
|- AppIcon.vue
components/
|- VButton.vue
|- VTable.vue
|- VIcon.vue
```

#### 2.9 单例组件名

**只应该拥有单个活跃实例的组件应该以 The 前缀命名，以示其唯一性。**

这不意味着组件只可用于一个单页面，而是每个页面只使用一次。这些组件永远不接受任何`prop`，因为它们是为你的应用定制的，而不是它们在你的应用中的上下文。如果你发现有必要添加`prop`，那就表明这实际上是一个可复用的组件，只是目前在每个页面里只使用一次。

```
// bad
components/
|- Heading.vue
|- MySidebar.vue

// good
components/
|- TheHeading.vue
|- TheSidebar.vue
```

#### 2.10 紧密耦合的组件名

**和父组件紧密耦合的子组件应该以父组件名作为前缀命名。**

如果一个组件只在某个父组件的场景下有意义，这层关系应该体现在其名字上。因为编辑器通常会按字母顺序组织文件，所以这样做可以把相关联的文件排在一起。

```
// bad
components/
|- TodoList.vue
|- TodoItem.vue
|- TodoButton.vue
components/
|- SearchSidebar.vue
|- NavigationForSearchSidebar.vue

// good
components/
|- TodoList.vue
|- TodoListItem.vue
|- TodoListItemButton.vue
components/
|- SearchSidebar.vue
|- SearchSidebarNavigation.vue
```

#### 2.11 组件名中的单词顺序

**组件名应该以高级别的 (通常是一般化描述的) 单词开头，以描述性的修饰词结尾。**

```
// bad
components/
|- ClearSearchButton.vue
|- ExcludeFromSearchInput.vue
|- LaunchOnStartupCheckbox.vue
|- RunSearchButton.vue
|- SearchInput.vue
|- TermsCheckbox.vue

// good
components/
|- SearchButtonClear.vue
|- SearchButtonRun.vue
|- SearchInputQuery.vue
|- SearchInputExcludeGlob.vue
|- SettingsCheckboxTerms.vue
|- SettingsCheckboxLaunchOnStartup.vue
```

#### 2.12 自闭合组件

**在单文件组件、字符串模板和 JSX 中没有内容的组件应该是自闭合的——但在 DOM 模板里永远不要这样做。**

自闭合组件表示它们不仅没有内容，而且刻意没有内容。其不同之处就好像书上的一页白纸对比贴有“本页有意留白”标签的白纸。而且没有了额外的闭合标签，你的代码也更简洁。

不幸的是，HTML 并不支持自闭合的自定义元素——只有官方的“空”元素。所以上述策略仅适用于进入 DOM 之前 Vue 的模板编译器能够触达的地方，然后再产出符合 DOM 规范的 HTML。

```html
//bad
<!-- 在单文件组件、字符串模板和 JSX 中 -->
<MyComponent></MyComponent>
<!-- 在 DOM 模板中 -->
<my-component />

// good
<!-- 在单文件组件、字符串模板和 JSX 中 -->
<MyComponent />
<!-- 在 DOM 模板中 -->
<my-component></my-component>
```

#### 2.13 完整单词的组件名

**组件名应该倾向于完整单词而不是缩写。**

编辑器中的自动补全已经让书写长命名的代价非常之低了，而其带来的明确性却是非常宝贵的。不常用的缩写尤其应该避免。

```
// bad
components/
|- SdSettings.vue
|- UProfOpts.vue

// good
components/
|- StudentDashboardSettings.vue
|- UserProfileOptions.vue
```

#### 2.14 Prop 名大小写

**在声明`prop`的时候，其命名应该始终使用`camelCase`，而在模板和`JSX`中应该始终使用`kebab-case`。**

我们单纯的遵循每个语言的约定。在 JavaScript 中更自然的是`camelCase`。而在 HTML 中则是`kebab-case`。

```html
// bad props: { 'greeting-text': String }
<WelcomeMessage greetingText="hi" />

// good props: { greetingText: String }
<WelcomeMessage greeting-text="hi" />
```

#### 2.15 多个特性的元素

**多个特性的元素应该分多行撰写，每个特性一行。**

在 JavaScript 中，用多行分隔对象的多个属性是很常见的最佳实践，因为这样更易读。模板和`JSX`值得我们做相同的考虑。

```html
// bad
<img src="https://vuejs.org/images/logo.png" alt="Vue Logo" />
<MyComponent foo="a" bar="b" baz="c" />

// good
<img src="https://vuejs.org/images/logo.png" alt="Vue Logo" />
<MyComponent foo="a" bar="b" baz="c" />
```

#### 2.16 模板中简单的表达式

**组件模板应该只包含简单的表达式，复杂的表达式则应该重构为计算属性或方法。**

复杂表达式会让你的模板变得不那么声明式。我们应该尽量描述应该出现的是什么，而非如何计算那个值。而且计算属性和方法使得代码可以重用。

```javascript
// bad
{{
  fullName.split(' ').map(function (word) {
    return word[0].toUpperCase() + word.slice(1)
  }).join(' ')
}}

// good
<!-- 在模板中 -->
{{ normalizedFullName }}
// 复杂表达式已经移入一个计算属性
computed: {
  normalizedFullName: function () {
    return this.fullName.split(' ').map(function (word) {
      return word[0].toUpperCase() + word.slice(1)
    }).join(' ')
  }
}
```

#### 2.17 简单的计算属性

**应该把复杂计算属性分割为尽可能多的更简单的属性。**

```javascript
// bad
computed: {
  price: function () {
    var basePrice = this.manufactureCost / (1 - this.profitMargin)
    return (
      basePrice -
      basePrice * (this.discountPercent || 0)
    )
  }
}

// good
computed: {
  basePrice: function () {
    return this.manufactureCost / (1 - this.profitMargin)
  },
  discount: function () {
    return this.basePrice * (this.discountPercent || 0)
  },
  finalPrice: function () {
    return this.basePrice - this.discount
  }
}
```

#### 2.18 带引号的特性值

**非空 HTML 特性值应该始终带引号 (单引号或双引号，选你 JS 里不用的那个)。**

在 HTML 中不带空格的特性值是可以没有引号的，但这鼓励了大家在特征值里不写空格，导致可读性变差。

```html
// bad
<input type="text" />
<AppSidebar :style={width:sidebarWidth+'px'}> // good
<input type="text" />
<AppSidebar :style="{ width: sidebarWidth + 'px' }"></AppSidebar>
```

#### 2.19 指令缩写

**指令缩写 (用`:`表示 `v-bind:` 、用`@`表示`v-on:` 和用`#`表示`v-slot:`)，应该要么都用要么都不用。**

```html
// bad
<input v-bind:value="newTodoText" :placeholder="newTodoInstructions" />
<input v-on:input="onInput" @focus="onFocus" />
<template v-slot:header>
  <h1>Here might be a page title</h1>
</template>

<template #footer>
  <p>Here's some contact info</p>
</template>

// good
<input :value="newTodoText" :placeholder="newTodoInstructions" />
<input v-bind:value="newTodoText" v-bind:placeholder="newTodoInstructions" />
<input @input="onInput" @focus="onFocus" />
<input v-on:input="onInput" v-on:focus="onFocus" />
<template v-slot:header>
  <h1>Here might be a page title</h1>
</template>

<template v-slot:footer>
  <p>Here's some contact info</p>
</template>
<template #header>
  <h1>Here might be a page title</h1>
</template>

<template #footer>
  <p>Here's some contact info</p>
</template>
```

#### 2.20 没有在 `v-if`/`v-else-if`/`v-else` 中使用`key`

**如果一组 `v-if` + `v-else` 的元素类型相同，最好使用 `key` (比如两个 `<div>` 元素)。**

默认情况下，Vue 会尽可能高效的更新 DOM。这意味着其在相同类型的元素之间切换时，会修补已存在的元素，而不是将旧的元素移除然后在同一位置添加一个新元素。如果本不相同的元素被识别为相同，则会出现意料之外的结果。

```html
// bad
<div v-if="error">
  错误：{{ error }}
</div>
<div v-else>
  {{ results }}
</div>

// good
<div v-if="error" key="search-status">
  错误：{{ error }}
</div>
<div v-else key="search-results">
  {{ results }}
</div>
```

#### 2.21 `scoped`中的元素选择器

**元素选择器应该避免在 `scoped` 中出现。**

在 `scoped` 样式中，类选择器比元素选择器更好，因为大量使用元素选择器是很慢的。

```html
// bad
<template>
  <button>X</button>
</template>

<style scoped>
  button {
    background-color: red;
  }
</style>

// good
<template>
  <button class="btn btn-close">X</button>
</template>

<style scoped>
  .btn-close {
    background-color: red;
  }
</style>
```

#### 2.22 隐性的父子组件通信

**应该优先通过 prop 和事件进行父子组件之间的通信，而不是 `this.$parent` 或改变 `prop`。**

一个理想的 Vue 应用是 `prop` 向下传递，事件向上传递的。遵循这一约定会让你的组件更易于理解。然而，在一些边界情况下 `prop` 的变更或 `this.$parent` 能够简化两个深度耦合的组件。

问题在于，这种做法在很多简单的场景下可能会更方便。但请当心，不要为了一时方便 (少写代码) 而牺牲数据流向的简洁性 (易于理解)。

```javascript
// bad
Vue.component('TodoItem', {
  props: {
    todo: {
      type: Object,
      required: true,
    },
  },
  template: '<input v-model="todo.text">',
})

Vue.component('TodoItem', {
  props: {
    todo: {
      type: Object,
      required: true,
    },
  },
  methods: {
    removeTodo() {
      var vm = this
      vm.$parent.todos = vm.$parent.todos.filter(function (todo) {
        return todo.id !== vm.todo.id
      })
    },
  },
  template: `
    <span>
      {{ todo.text }}
      <button @click="removeTodo">
        X
      </button>
    </span>
  `,
})

// good
Vue.component('TodoItem', {
  props: {
    todo: {
      type: Object,
      required: true,
    },
  },
  template: `
    <input
      :value="todo.text"
      @input="$emit('input', $event.target.value)"
    >
  `,
})

Vue.component('TodoItem', {
  props: {
    todo: {
      type: Object,
      required: true,
    },
  },
  template: `
    <span>
      {{ todo.text }}
      <button @click="$emit('delete')">
        X
      </button>
    </span>
  `,
})
```

### 3. Vuex 规范

#### 1. 基本规则

Vuex 并不限制你的代码结构。但是，它规定了一些需要遵守的规则：

1. 应用层级的状态应该集中到单个 store 对象中。

2. 提交 mutation 是更改状态的唯一方法，并且这个过程是同步的。

3. 异步逻辑都应该封装到 action 里面。

只要你遵守以上规则，如何组织代码随你便。如果你的 store 文件太大，只需将 action、mutation 和 getter 分割到单独的文件。

#### 2. Mutation

- 提交载荷（Payload）

```javascript
// ...
mutations: {
  increment (state, n) {
    state.count += n
  }
}
store.commit('increment', 10)
```

在大多数情况下，载荷应该是一个对象，这样可以包含多个字段并且记录的 mutation 会更易读：

```javascript
// ...
mutations: {
  increment (state, payload) {
    state.count += payload.amount
  }
}
store.commit('increment', {
  amount: 10
})
```

- 遵守 Vue 的响应规则

既然 Vuex 的 store 中的状态是响应式的，那么当我们变更状态时，监视状态的 Vue 组件也会自动更新。这也意味着 Vuex 中的 mutation 也需要与使用 Vue 一样遵守一些注意事项：

1. 最好提前在你的 store 中初始化好所有所需属性。

2. 当需要在对象上添加新属性时，你应该

- 使用 Vue.set(obj, 'newProp', 123), 或者

- 以新对象替换老对象。例如，利用 stage-3 的对象展开运算符我们可以这样写：

```javscript
state.obj = { ...state.obj, newProp: 123 }
```

- 使用常量替代 Mutation 事件类型

使用常量替代 mutation 事件类型在各种 Flux 实现中是很常见的模式。这样可以使 linter 之类的工具发挥作用，同时把这些常量放在单独的文件中可以让你的代码合作者对整个 app 包含的 mutation 一目了然：

```javascript
// mutation-types.js
export const SOME_MUTATION = 'SOME_MUTATION'
// store.js
import Vuex from 'vuex'
import { SOME_MUTATION } from './mutation-types'

const store = new Vuex.Store({
  state: { ... },
  mutations: {
    // 我们可以使用 ES2015 风格的计算属性命名功能来使用一个常量作为函数名
    [SOME_MUTATION] (state) {
      // mutate state
    }
  }
})
```

用不用常量取决于你——在需要多人协作的大型项目中，这会很有帮助。但如果你不喜欢，你完全可以不这样做。（建议在同一个项目中，要么都用常量替代，要么就都不用）

## 配置文件

**在项目开发时，各个配置文件请不要轻易改动。（如需改动，请告知项目中的前端开发同事）**

- **.browserslistrc（目标浏览器范围）**

```
> 1%            // 全球使用统计选择的浏览器版本，使用率大于1%
last 2 versions // 所有浏览器兼容到最后两个版本根据caniuse追踪的版本
not ie <= 8     // 最低兼容到ie9
```

- **.editorconfig（IDE 编码格式配置）**

```
[*.{js,jsx,ts,tsx,vue}]          // 匹配的文件格式
indent_style = space.            // 缩进方式为空格
indent_size = 2                  // 缩进为两个空格
trim_trailing_whitespace = true  // 删除一行中的前后空格
insert_final_newline = true      // 在文件结尾插入新行
```

- **.env（环境变量配置）**

```
VUE_APP_I18N_LOCALE=cn                          // 当前语言环境
VUE_APP_I18N_FALLBACK_LOCALE=en                 // 默认语言环境
VUE_APP_API_SERVER=https://www.development.com  // 服务端接口host
```

- **.eslintrc（eslint 配置）**

```javascript
module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ['plugin:vue/essential', '@vue/standard'],
  rules: {
    // 'generator-star-spacing': 'off',
    // // allow debugger during development
    // 'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // allow paren-less arrow functions
    'arrow-parens': 0,
    // allow async-await
    'generator-star-spacing': 0,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    // 打包生产环境禁止用console
    'no-console': process.env.NODE_ENV === 'production' ? 2 : 0,
    //禁止使用alert confirm prompt
    'no-alert': process.env.NODE_ENV === 'production' ? 2 : 0,
    // 函数名与其后面的括号之间可以无空格
    'space-before-function-paren': ['error', 'never'],
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
}
```

- **.gitignore（git 上传忽略）**

```
.DS_Store
node_modules
/dist
config/config.js

# local env files
.env.local
.env.*.local

# Log files
npm-debug.log*
yarn-debug.log*
yarn-error.log*

# Editor directories and files
.idea
.vscode
*.suo
*.ntvs*
*.njsproj
*.sln
*.sw?
```

- **babel.config.js（babel 配置）**

```javascript
module.exports = {
  presets: ['@vue/app'],
  plugins: [
    [
      'component',
      {
        libraryName: 'element-ui',
        styleLibraryName: 'theme-chalk',
      },
    ],
  ],
}
```

- **postcss.config.js（postcss 配置）**

```javascript
module.exports = {
  plugins: {
    autoprefixer: {},
  },
}
```

- **vue.config.js（全局配置）**

```javascript
const path = require('path')

module.exports = {
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.join(__dirname, 'src'),
      },
    },
  },
  pluginOptions: {
    i18n: {
      locale: 'cn',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: true,
    },
  },
  devServer: {
    proxy: {
      '^/prefix_need_to_proxy': {
        target: '<url>',
        ws: true,
        changeOrigin: true,
      },
    },
  },
}
```

## 分支管理

所有开发代码应提交到`git`中，版本管理原则如下：

1. 项目默认会拥有`master`分支和`development`分支，在版本稳定时，两个分支应保持业务代码一致；

2. 开发新模块或者新功能时，从`development`生成新的分支，对应命名为`feature/xxx`，在提测时将对应的`feature/xxx`分支合并到`development`分支，以准备测试；

3. 项目测试通过，需要上线时，将`development`分支合并到`master`分支，以准备上线；

4. 修复线上紧急 bug 时，从`master`生成新的分支，对应命名为`hotfix/xxx`，在提测时将对应`hotfix/xxx`分支合并到`master`，以供测试；
